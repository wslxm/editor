# markdown-editor

## 介绍
Markdown编辑器  editor ，基于 editor.md 优化cv图片上传和图片拖拽上传

## 快速使用

拉取改代码放置自己的静态资源路径，按照下方示例配置即可



## 对比editor 该源码修改部分

1、相比原代码增加 imgCopyPull2.js ,用于支持qq截图+微信截图文件上传 和 多文件拖拽上传文件

2、增加本地 emoji 表情，

在  `\plugins\emoji-dialog\emoji` 目录


![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/182838_74572d7e_2208600.png "屏幕截图.png")

editormd.js 中修改了表情读取位置 ，  3368 行处

![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/182933_551566d3_2208600.png "屏幕截图.png")



## 使用说明

### 1、编辑器相关配置

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Markdown编辑器</title>
</head>
<!-- layui样式 -->
<link rel="stylesheet" href="/components/editor/css/editormd.css">
<script src="/base/js/app.js"></script>
<script src='/base/js/jquery-3.4.1.min.js'></script>
<script type="text/javascript" src="/components/editor/editormd.js"></script>
<!-- 图片粘贴复制拖拽，Markdown编辑器元素  textarea id必须为 L_content，并且该引入必须在 textarea的下方，否则无法读取到 L_content 参数 -->
<script src="/components/editor/imgCopyPull2.js"></script>

<body>
<div id="L_content"></div>
<script>
    //注意1：这里的就是上面的DIV的id属性值
    var myEditor = editormd("L_content", {
        width: "90%",
        height: 640,
        syncScrolling: "single",
        path: "/components/editor/lib/",// 资源路径
        // 自定义的增强配置！
        saveHTMLToTextarea: true,        // 保存 HTML 到 Textarea
        emoji: true, // 开启表情的功能！ 图片的本地配置！
        // theme: "light",               // 工具栏主题
        // previewTheme: "dark",         // 预览主题
        // editorTheme: "pastel-on-dark",// 编辑主题
        // markdown的配置！
        tex: true,                   // 开启科学公式TeX语言支持，默认关闭
        flowChart: true,             // 开启流程图支持，默认关闭
        sequenceDiagram: true,       // 开启时序/序列图支持，默认关闭,
        imageUpload: false,          // 图片本地上传,禁用，使用imgCopyPull2.js 的上传方式
        /*指定需要显示的功能按钮*/
        toolbarIcons : function() {
            return ["undo","redo","|",
                "bold","del","italic","quote","ucwords","uppercase","lowercase","|",
                // "h1","h2","h3","h4","h5","h6","|",
                "list-ul","list-ol","hr","|",
                "link","reference-link","image","code","preformatted-text",
                "code-block","table","datetime","emoji","html-entities","pagebreak","|",
                "goto-line","watch","preview","fullscreen","clear","search","|",
                "help","info","|",
                "releaseIcon", "index"]
        },
        /*自定义功能按钮，下面我自定义了2个，一个是发布，一个是返回首页*/
        toolbarIconTexts : {
            releaseIcon : "<span bgcolor=\"gray\">发布文章</span>",
           // index : "<span bgcolor=\"red\">返回首页</span>",
        },
        /*给自定义按钮指定回调函数*/
        toolbarHandlers:{
            releaseIcon : function(cm, icon, cursor, selection) {
                //表单提交
                // myEditor.method = "post";
                // myEditor.action = "/article/addArticle";//提交至服务器的路径
                // myEditor.submit();
                alert("发布")
            },
            // index : function(){
            //     // window.location.href = '/';
            //     alert("返回首页")
            // },
        }
    });
</script>
</body>
</html>
```

### 2、获取数据


```
## 方式1，通过调用方法获取（推荐）
// myEditor .getMarkdown();       // 获取 Markdown 源码
// myEditor .getHTML();           // 获取 Textarea 保存的 HTML 源码
// myEditor .getPreviewedHTML();  // 获取预览窗口里的 HTML，在开启 watch 且没有开启 saveHTMLToTextarea 时使用


## 方式2，jquery的类选择器进行获取（无法在编辑器渲染成功的回调中使用 jquery 给 textarea 输入框赋值的方式进行回显）：
var htmlContent = $('.editormd-html-textarea').val();
var mdContent = $('.editormd-markdown-textarea').val();
```


### 3、数据回显

```
## 方式1
myEditor.setMarkdown(md);

# 方式2 jquery 方式
$('#content').val(parent.data.content);
```

### 4、解析 markdown 源码显示 HTML 示例

```
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Markdown编辑器</title>
</head>
<!-- 页面解析markdown为HTML显示需要的css -->
<link rel="stylesheet" href="/components/editor/css/editormd.css">
<script src='/base/js/jquery-3.4.1.min.js'></script>
<script type="text/javascript" src="/components/editor/editormd.js"></script>

<link rel="stylesheet" href="/components/editor/css/editormd.preview.min.css"/>
<!-- markdown解析成HTML显示需要的js -->
<script src="/components/editor/lib/marked.min.js"></script>
<script src="/components/editor/lib/prettify.min.js"></script>
<script src="/components/editor/lib/raphael.min.js"></script>
<script src="/components/editor/lib/underscore.min.js"></script>
<script src="/components/editor/lib/sequence-diagram.min.js"></script>
<script src="/components/editor/lib/flowchart.min.js"></script>
<script src="/components/editor/lib/jquery.flowchart.min.js"></script>
<body>
<!-- 页面解析markdown为HTML显示 -->
<div id="markdownToHTML" style="margin-left: 100px">
    <textarea id="content" style="display:none;" placeholder="markdown语言">
          <!-- 内容区 -->
    </textarea>
</div>


<script>
    // 给textarea赋值
    // $('#content').val();
    // 解析, 重新赋值后需重新调优 editormd.markdownToHTM 来重新解析
    editormd.markdownToHTML("markdownToHTML", {
        htmlDecode: "style,script,iframe",
        emoji: true,  // 解析表情
        taskList: true,  // 解析列表
        tex: true,  // 默认不解析
        flowChart: true,  // 默认不解析
        sequenceDiagram: true  // 默认不解析
    });
</script>
</body>

</html>
```



